import React from 'react';
import {Component} from 'react';
import { Control, LocalForm, Errors } from 'react-redux-form';
import {Loading} from './LoadingComponent';
import { Card,CardImg,CardHeader,CardText,CardBody,CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalHeader, ModalBody,Row, Col, Label } from 'reactstrap';
import {baseUrl} from '../Shared/baseUrl';
import { Link } from 'react-router-dom';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

  class CommentForm extends Component{
    constructor(props){
        super(props);
        this.state={
            isModalOpen : false,
            name : '',
            rating: '0',
            comments:'',
            touched : {
                name: false
            }
        }

        this.toggleModal = this.toggleModal.bind(this);
        this.handleCommentSubmit = this.handleCommentSubmit.bind(this);
       
    }

    toggleModal(){
        
        this.setState({
            isModalOpen : !this.state.isModalOpen
        });
    }
    handleCommentSubmit(values) {
        this.toggleModal();
        this.props.postComment(this.props.dishId, values.rating, values.name, values.comments)
    this.setState({
        isModalOpen : false
    })
   }
    render(){
    return(
        <React.Fragment>
        <div className = "container">
            <Button className="bg-success" onClick={this.toggleModal}>Add Comment</Button>
        </div>
        <Modal isOpen= {this.state.isModalOpen} toggle={this.toggleModal}>
                <ModalHeader toggle={this.toggleModal}>Add Comment</ModalHeader>
                <ModalBody>
                <LocalForm onSubmit={(values) => this.handleCommentSubmit(values)}>
                   <Row className="form-group">
                                <Label htmlFor="name" md={2}>First Name</Label>
                                <Col md={10}>
                                    <Control.text model=".name" id="name" name="name"
                                        placeholder="Name"
                                        className="form-control"
                                        validators={{
                                            required, maxLength: maxLength(15), minLength: minLength(3)
                                           }}
                                         />
                                         <Errors
                                        className="text-danger"
                                        model=".name"
                                        show='touched'
                                        messages={{
                                            required: 'Required ',
                                            minLength: 'Must be greater than 2 characters ',
                                            maxLength: 'Must be 15 characters or less '
                                        }}
                                     />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="rating" md={2}>Rating</Label>
                                <Col md={{size: 5}}>
                                    <Control.select model=".rating" name="rating"
                                        className="form-control">
                                        <option>0</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Control.select>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="comments" md={2}>Your Feedback</Label>
                                <Col md={10}>
                                    <Control.textarea model=".comments" id="comments" name="comments"
                                        rows="6"
                                        className="form-control"
                                         />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={{size:10, offset: 2}}>
                                    <Button type="submit" color="primary">
                                      Submit
                                    </Button>
                                </Col>
                            </Row>
                   </LocalForm>
                </ModalBody>
            </Modal>
            </React.Fragment>
    );
   }
  }

  function RenderDish({dish}){
        if(dish){
            return(
        <FadeTransform in transformProps={{exitTransform: 'scale(0.5) translateY(-50%)'}}>
            <Card>
                <CardImg top src={baseUrl + dish.image} alt={dish.name} />
                <CardBody>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>
        </FadeTransform>
            
            );
        }
        return (
            <div></div>
        );
    }
      
  function RenderComments({comments, postComment, dishId}){  
        
        if(comments){
            return(
            <Card style={{backgroundColor:"white"}}>
                <CardHeader>
                    <h1>Comments</h1>
                </CardHeader>
                <CardBody>
                
                <div style={{textAlign:"left"}}>
                <Stagger in>
                    {comments.map((data)=>{
                        return(
                        <Fade in>
                        <div key={data.id}>
                            <h4>User_{data.id+1} rates it {data.rating} stars.</h4>
                            <p>{data.id+1}*1 {data.comment}</p>
                            <p>{data.id+1}*2 {data.author}</p>
                            <p>{data.id+1}*3 {new Intl.DateTimeFormat('en-US',{year:'numeric',month:'short',day:'2-digit'}).format(new Date(Date.parse(data.date)))}</p>
                        </div>
                        </Fade>
                        );
                    })}
                    </Stagger>
                </div>                
                <CommentForm dishId={dishId} postComment={postComment}/>                  
                </CardBody>
            </Card>
            );
        }
        return (
            <div></div>
        );
    }
    
    const DishDetail =(props) =>{
         if(props.isLoading){
             return(
                 <div className = "container">
                     <div className = "row">
                         <Loading/>
                     </div>
                 </div>
             );
         }
         else if (props.errMess){
            return(
                <div className = "container">
                    <div classNAme = "row">
                       <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
         }
         else if (props.dish){
        return (
            <div className="container">
            <div className="row">
                <Breadcrumb>

                    <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{props.dish.name}</h3>
                    <hr />
                </div>                
            </div>
            <div className="row">
                <div className="col-12 col-md-5 m-1">
                    <RenderDish dish={props.dish} />
                </div>
                <div className="col-12 col-md-5 m-1">
                    <RenderComments comments={props.comments}
                    postComment = {props.postComment}
                    dishId = {props.dish.id} />
                </div>
            </div>
            </div>
        );
      }
    }
    

export default DishDetail;
